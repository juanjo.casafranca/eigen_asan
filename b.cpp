#include "a.h"

std::uint8_t fooB() {
  Eigen::Matrix<Eigen::Vector<std::uint8_t, 3>, Eigen::Dynamic, Eigen::Dynamic>
      input{Eigen::Matrix<Eigen::Vector<std::uint8_t, 3>, Eigen::Dynamic,
                          Eigen::Dynamic>::
                Constant(12, 12, Eigen::Vector<std::uint8_t, 3>::Constant(1))};

  Eigen::Matrix<Eigen::Vector<std::uint8_t, 3>, Eigen::Dynamic, Eigen::Dynamic>
      out = fooA(input);

  return out.coeff(0, 0)[0];
}
