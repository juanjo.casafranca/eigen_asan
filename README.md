# Eigen Asan

Example of a crash due to an heap buffer overflow detected by ASAN. 

A matrix is allocated in a library which doesn't have ASAN enabled and its returned to another library which has ASAN enabled.
When the matrix is destroyed, which happens in the library with ASAN enabled, the heap_buffer_overflow occurs. 

Eigen calls handmade_aligned_mallic and handmade_aligned_free for allocating and freeing. 
